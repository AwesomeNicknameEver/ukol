<?php


namespace App\Model;

use Nette\Application\UI;

class TreeMenuControl extends UI\Control {
	private $menuItems;

	public function __construct($menuItems) {
		$this->menuItems = $menuItems;
	}

	public function render(): void
	{
		// we will put some parameters into the template
		$this->template->items = $this->menuItems;
		// and draw it
		$this->template->render(__DIR__ . '/../Presenters/templates/TreeManager/TreeMenu.latte');
	}

	public function handleclickItemTreeMenu($id, $name){

	}

}