<?php


namespace App\Model;

use Nette;

class TreeMenuManager {

	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	public function getMenuItems() {
		return $this->getSubItems(null, 0);
	}

	private function getSubItems($parent_id, $level) {
		$items = $this->database->table('treemenu')->select('id, name, level, order, parrent_id')
			->where('level', $level)
			->where('parrent_id', $parent_id)->order('order ASC')->fetchAll();

		$all_items = [];

		foreach ($items as $item) {
			$all_items[] = $item;
			$sub_items = $this->getSubItems($item['id'], $level + 1);
			foreach ($sub_items as $sub_item){
				$all_items[] = $sub_item;
			}
		}
		return $all_items;
	}
}