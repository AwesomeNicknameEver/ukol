<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\TreeMenuControl;
use Nette;
use App\Model\TreeMenuManager;
use Nette\Application\UI;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
	/** @var TreeMenuManager */
	private $tmManager;

	public function __construct(TreeMenuManager $tmManager)
	{
		$this->tmManager = $tmManager;
		$this->getComponent('treeMenu');
	}

	public function renderDefault(): void
	{

	}

	protected function createComponentTreeMenu(){
		$items = $this->tmManager->getMenuItems();
		$control = new TreeMenuControl($items);
		return $control;
	}

}
